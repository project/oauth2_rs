OAuth2 Resource server Drupal implementation.

This extension allows Drupal to play the role of Resource server in particular scenario where the authorization server a separate entity which is able to release JWT token, such as:

AWS Cognito
Keycloak
RFC6749

resource server
The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens.

You can dig into specification of protocol if necessary. https://tools.ietf.org/html/rfc6749#page-7

(D) The authorization server authenticates the client and validates the authorization grant, and if valid, issues an access token.
(E) The client requests the protected resource from the resource server and authenticates by presenting the access token.
(F) The resource server validates the access token, and if valid, serves the request.

Credits

Big thanks for inspiration to authors of league/oauth2-server
and drupal/simple_oauth. Biggest part of this particular project was reused&adapted from these two.
