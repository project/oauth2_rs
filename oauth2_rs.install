<?php

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Add roles field to consumer.
 */
function oauth2_rs_update_8000() {
  $roles_field = BaseFieldDefinition::create('entity_reference')
    ->setLabel(new TranslatableMarkup('Scopes'))
    ->setDescription(new TranslatableMarkup('The roles for this Consumer. OAuth2 scopes are implemented as Drupal roles.'))
    ->setRevisionable(TRUE)
    ->setSetting('target_type', 'user_role')
    ->setSetting('handler', 'default')
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setTranslatable(FALSE)
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'entity_reference_label',
      'weight' => 5,
    ])
    ->setDisplayOptions('form', [
      'type' => 'options_buttons',
      'weight' => 5,
    ]);
  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('roles', 'consumer', 'oauth2_rs', $roles_field);
}
