<?php

namespace Drupal\oauth2_rs\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Authorization validator plugin item annotation object.
 *
 * Plugin Namespace: Plugin\AuthorizationValidatorPlugin.
 *
 * @see \Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class AuthorizationValidator extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
