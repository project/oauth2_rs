<?php

namespace Drupal\oauth2_rs\Authentication\Provider;

use Drupal\consumers\Entity\Consumer;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationBrowser;
use Drupal\oauth2_rs\Authentication\TokenAuthUser;
use Drupal\oauth2_rs\AuthorizationValidators\CognitoBearerValidator;
use Drupal\oauth2_rs\PageCache\OauthRequestPolicyInterface;
use Drupal\oauth2_rs\Server\ResourceServerInterface;
use Drupal\oauth2_rs\Service\UserAuthManagerInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class JWT Authentication provider..
 */
class JWTAuthenticationProvider implements AuthenticationProviderInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Resource server.
   *
   * @var \Drupal\oauth2_rs\Server\ResourceServerInterface
   */
  protected $resourceServer;

  /**
   * Cache policy.
   *
   * @var \Drupal\oauth2_rs\PageCache\SimpleOauthRequestPolicyInterface
   */
  protected $oauthPageCacheRequestPolicy;

  /**
   * User Auth manager.
   *
   * @var \Drupal\oauth2_rs\Service\UserAuthManager
   */
  protected $userAuthManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  public const OAUTH_ACCESS_TOKEN_ID = 'oauth_access_token_id';

  public const OAUTH_CLIENT_ID = 'oauth_client_id';

  public const OAUTH_USER_ID = 'oauth_user_id';

  public const OAUTH_SCOPE = 'oauth_scope';

  public const OAUTH_USERNAME = 'oauth_username';

  public const OAUTH_ID_TOKEN_EMAIL = 'oauth_user_email';

  public const OAUTH_ID_TOKEN_USERNAME = 'oauth_id_token_username';

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\oauth2_rs\Server\ResourceServerInterface $resource_server
   *   The resource server object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\oauth2_rs\PageCache\OauthRequestPolicyInterface $page_cache_request_policy
   *   The page cache request policy.
   * @param \Drupal\oauth2_rs\Service\UserAuthManagerInterface $user_auth_manager
   *   User Auth manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack.
   */
  public function __construct(
    ResourceServerInterface $resource_server,
    EntityTypeManagerInterface $entity_type_manager,
    OauthRequestPolicyInterface $page_cache_request_policy,
    UserAuthManagerInterface $user_auth_manager,
    ConfigFactoryInterface $config_factory,
    LanguageManagerInterface $language_manager,
    RequestStack $request_stack
  ) {
    $this->resourceServer = $resource_server;
    $this->entityTypeManager = $entity_type_manager;
    $this->oauthPageCacheRequestPolicy = $page_cache_request_policy;
    $this->userAuthManager = $user_auth_manager;
    $this->config = $config_factory->get('oauth2_rs');
    $this->languageManager = $language_manager;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    // The request policy service won't be used in case of non GET or HEAD
    // methods so we have to explicitly call it.
    /* @see \Drupal\Core\PageCache\RequestPolicy\CommandLineOrUnsafeMethod::check() */
    return $this->oauthPageCacheRequestPolicy->isOauth2Request($request);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  public function authenticate(Request $request) {
    // Update the request with the OAuth information.
    try {
      $authRequest = $this->resourceServer->validateAuthenticatedRequest($request);
    } catch (OAuthServerException $exception) {
      // Procedural code here is hard to avoid.
      watchdog_exception('oauth2_rs', $exception);
      throw new HttpException(
        $exception->getHttpStatusCode(),
        $exception->getHint(),
        $exception
      );
    }
    // Get Token from request.
    $token = $authRequest->getAttribute('token');

    // Get params from Request object, was set from Token Claim.
    $email = $authRequest->getAttribute(self::OAUTH_ID_TOKEN_EMAIL);
    $username = $authRequest->getAttribute(self::OAUTH_USERNAME);
    $tokenRoles = $authRequest->getAttribute(self::OAUTH_SCOPE);

    // Is user doesn't exist by name with access token. I need both of them.
    $account = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $username]);

    // IdToken is required for new user creation.
    if (empty($account) && empty($email)) {
      $exception = OAuthServerException::invalidRequest('IdToken');
      throw new HttpException(
        $exception->getHttpStatusCode(),
        $exception->getMessage().' '.$exception->getHint(),
        $exception
      );
    }

    $consumer = $this->userAuthManager->getUserConsumerByRole($tokenRoles);
    // Throw exception if consumer is not related with role.
    if (!$consumer instanceof Consumer) {
      $scopeMessage = implode(', ', $tokenRoles);
      $exception = OAuthServerException::invalidScope($scopeMessage);
      throw new HttpException(
        $exception->getHttpStatusCode(),
        $exception->getMessage() .'. '. $exception->getHint(),
        $exception
      );
    }

    // Get language code from request.
    $language = 'en';
    // There no negotiator if only one language available.
    if ($this->languageManager->isMultilingual()) {
      $negotiation_method = $this->languageManager->getNegotiator()
        ->getNegotiationMethodInstance(LanguageNegotiationBrowser::METHOD_ID);
      $language = $negotiation_method->getLangcode($this->currentRequest) ?? 'en';
    }
    $account = reset($account);
    // Create new User on very first request.
    if (empty($account)) {
      $account = $this->userAuthManager->createAccount($email, $username, $tokenRoles, $language);
    }

    // Create decorated User object with token information.
    $account = new TokenAuthUser($account, $token, $consumer);

    // TODO consider to put login finalize into own service and inject here.
    user_login_finalize($account);
    // Revoke the access token for the blocked user.
    if ($account->isBlocked() && $account->isAuthenticated()) {
      $exception = OAuthServerException::accessDenied($this->t(
        '%name is blocked or has not been activated yet.',
        ['%name' => $account->getAccountName()]
      )
      );
      watchdog_exception('oauth2_rs', $exception);
      throw new HttpException(
        $exception->getHttpStatusCode(),
        $exception->getHint(),
        $exception
      );
    }

    // Set consumer ID header on successful authentication, so negotiators
    // will trigger correctly.
    $request->headers->set($account->getConsumer()->uuid(), 'X-Consumer-ID');

    return $account;
  }

}
