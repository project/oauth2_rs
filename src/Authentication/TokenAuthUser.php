<?php

namespace Drupal\oauth2_rs\Authentication;

use Drupal\consumers\Entity\Consumer;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Lcobucci\JWT\Token;
use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * The decorated user class with token information.
 *
 * @internal
 */
class TokenAuthUser implements TokenAuthUserInterface {

  /**
   * The decorator subject.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The bearer token.
   *
   * @var \Lcobucci\JWT\Token
   */
  protected $token;

  /**
   * The activated consumer instance.
   *
   * @var \Drupal\consumers\Entity\Consumer
   */
  protected $consumer;

  /**
   * Constructs a TokenAuthUser object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User Account.
   * @param \Lcobucci\JWT\Token $token
   *   The underlying token.
   * @param \Drupal\consumers\Entity\Consumer $consumer
   *   Consumer object related with given User role.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   When there is no user.
   */
  public function __construct(AccountInterface $account, Token $token, Consumer $consumer) {
    if (!$token) {
      throw OAuthServerException::invalidClient();
    }

    $this->user = $account;
    $this->consumer = $consumer;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumer() {
    return $this->consumer;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles($exclude_locked_roles = FALSE) {
    // Get User Roles from Consumer, since it's already processed.
    $consumerRoles = $this->consumer->get('roles')->getValue();
    // Expect few roles might be assigned.
    $roles = array_map(function ($role) {
      return $role['target_id'];
    }, $consumerRoles);
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function hasPermission($permission) {
    // User #1 has all privileges.
    if ((int) $this->id() === 1) {
      return TRUE;
    }

    return $this->getRoleStorage()
      ->isPermissionInRoles($permission, $this->getRoles());
  }

  /**
   * Returns the role storage object.
   *
   * @return \Drupal\user\RoleStorageInterface
   *   The role storage object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function getRoleStorage() {
    /** @var \Drupal\user\RoleStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('user_role');
    return $storage;
  }

  /* ---------------------------------------------------------------------------
  All the methods below are delegated to the decorated user.
  --------------------------------------------------------------------------- */

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $this->user->access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function isAuthenticated() {
    return $this->user->isAuthenticated();
  }

  /**
   * {@inheritdoc}
   */
  public function isAnonymous() {
    return $this->user->isAnonymous();
  }

  /**
   * {@inheritdoc}
   */
  public function getPreferredLangcode($fallback_to_default = TRUE) {
    return $this->user->getPreferredLangcode($fallback_to_default);
  }

  /**
   * {@inheritdoc}
   */
  public function getPreferredAdminLangcode($fallback_to_default = TRUE) {
    return $this->user->getPreferredAdminLangcode($fallback_to_default);
  }

  /**
   * {@inheritdoc}
   */
  public function getUsername() {
    return $this->user->getUsername();
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountName() {
    return $this->user->getAccountName();
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayName() {
    return $this->user->getDisplayName();
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->user->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeZone() {
    return $this->user->getTimeZone();
  }

  /**
   * {@inheritdoc}
   */
  public function getLastAccessedTime() {
    return $this->user->getLastAccessedTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->user->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->user->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->user->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function hasTranslationChanges() {
    return $this->user->hasTranslationChanges();
  }

  /**
   * {@inheritdoc}
   */
  public function setRevisionTranslationAffected($affected) {
    return $this->user->setRevisionTranslationAffected($affected);
  }

  /**
   * {@inheritdoc}
   */
  public function isRevisionTranslationAffected() {
    return $this->user->isRevisionTranslationAffected();
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->user->getChangedTime();
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    return $this->user->setChangedTime($timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTimeAcrossTranslations() {
    return $this->user->getChangedTimeAcrossTranslations();
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return $this->user->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->user->id();
  }

  /**
   * {@inheritdoc}
   */
  public function language() {
    return $this->user->language();
  }

  /**
   * {@inheritdoc}
   */
  public function isNew() {
    return $this->user->isNew();
  }

  /**
   * {@inheritdoc}
   */
  public function enforceIsNew($value = TRUE) {
    return $this->user->enforceIsNew($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return $this->user->getEntityTypeId();
  }

  /**
   * {@inheritdoc}
   */
  public function bundle() {
    return $this->user->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->user->label();
  }

  /**
   * {@inheritdoc}
   */
  public function urlInfo($rel = 'canonical', array $options = []) {
    return $this->user->urlInfo($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function url($rel = 'canonical', $options = []) {
    return $this->user->url($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function link($text = NULL, $rel = 'canonical', array $options = []) {
    return $this->user->link($text, $rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function hasLinkTemplate($key) {
    return $this->user->hasLinkTemplate($key);
  }

  /**
   * {@inheritdoc}
   */
  public function uriRelationships() {
    return $this->user->uriRelationships();
  }

  /**
   * {@inheritdoc}
   */
  public static function load($id) {
    return User::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadMultiple(array $ids = NULL) {
    return User::loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    return User::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    return $this->user->save();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    $this->user->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->user->preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    $this->user->postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    User::preCreate($storage, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    return $this->user->postCreate($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    User::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    User::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    User::postLoad($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    return $this->user->createDuplicate();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    return $this->user->getEntityType();
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    return $this->user->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalId() {
    return $this->user->getOriginalId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    return $this->user->getCacheTagsToInvalidate();
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalId($id) {
    return $this->user->setOriginalId($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTypedData() {
    return $this->user->getTypedData();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyKey() {
    return $this->user->getConfigDependencyKey();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyName() {
    return $this->user->getConfigDependencyName();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTarget() {
    return $this->user->getConfigTarget();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    return User::baseFieldDefinitions($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    return User::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);
  }

  /**
   * {@inheritdoc}
   */
  public function hasField($field_name) {
    return $this->user->hasField($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition($name) {
    return $this->user->getFieldDefinition($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinitions() {
    return $this->user->getFieldDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this->user->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function get($field_name) {
    return $this->user->get($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function set($field_name, $value, $notify = TRUE) {
    return $this->user->set($field_name, $value, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getFields($include_computed = TRUE) {
    return $this->user->getFields($include_computed);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslatableFields($include_computed = TRUE) {
    return $this->user->getTranslatableFields($include_computed);
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($field_name) {
    $this->user->onChange($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    return $this->user->validate();
  }

  /**
   * {@inheritdoc}
   */
  public function isValidationRequired() {
    return $this->user->isValidationRequired();
  }

  /**
   * {@inheritdoc}
   */
  public function setValidationRequired($required) {
    return $this->user->setValidationRequired($required);
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheContexts(array $cache_contexts) {
    return $this->user->addCacheContexts($cache_contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheTags(array $cache_tags) {
    return $this->user->addCacheTags($cache_tags);
  }

  /**
   * {@inheritdoc}
   */
  public function mergeCacheMaxAge($max_age) {
    return $this->user->mergeCacheMaxAge($max_age);
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheableDependency($other_object) {
    return $this->user->addCacheableDependency($other_object);
  }

  /**
   * {@inheritdoc}
   */
  public function isNewRevision() {
    return $this->user->isNewRevision();
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($value = TRUE) {
    $this->user->setNewRevision($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionId() {
    return $this->user->getRevisionId();
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultRevision($new_value = NULL) {
    return $this->user->isDefaultRevision($new_value);
  }

  /**
   * {@inheritdoc}
   */
  public function preSaveRevision(EntityStorageInterface $storage, \stdClass $record) {
    $this->user->preSaveRevision($storage, $record);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultTranslation() {
    return $this->user->isDefaultTranslation();
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationLanguages($include_default = TRUE) {
    return $this->user->getTranslationLanguages($include_default);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslation($langcode) {
    return $this->user->getTranslation($langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function getUntranslated() {
    return $this->user->getUntranslated();
  }

  /**
   * {@inheritdoc}
   */
  public function hasTranslation($langcode) {
    return $this->user->hasTranslation($langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function addTranslation($langcode, array $values = []) {
    return $this->user->addTranslation($langcode, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function removeTranslation($langcode) {
    $this->user->removeTranslation($langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function isTranslatable() {
    return $this->user->isTranslatable();
  }

  /**
   * {@inheritdoc}
   */
  public function hasRole($rid) {
    return in_array($rid, $this->getRoles());
  }

  /**
   * {@inheritdoc}
   */
  public function addRole($rid) {
    $this->user->addRole($rid);
  }

  /**
   * {@inheritdoc}
   */
  public function removeRole($rid) {
    $this->user->removeRole($rid);
  }

  /**
   * {@inheritdoc}
   */
  public function setUsername($username) {
    return $this->user->setUsername($username);
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->user->getPassword();
  }

  /**
   * {@inheritdoc}
   */
  public function setPassword($password) {
    return $this->user->setPassword($password);
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    return $this->user->setEmail($mail);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->user->getCreatedTime();
  }

  /**
   * {@inheritdoc}
   */
  public function setLastAccessTime($timestamp) {
    return $this->user->setLastAccessTime($timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getLastLoginTime() {
    return $this->user->getLastLoginTime();
  }

  /**
   * {@inheritdoc}
   */
  public function setLastLoginTime($timestamp) {
    return $this->user->setLastLoginTime($timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->user->isActive();
  }

  /**
   * {@inheritdoc}
   */
  public function isBlocked() {
    return $this->user->isBlocked();
  }

  /**
   * {@inheritdoc}
   */
  public function activate() {
    return $this->user->activate();
  }

  /**
   * {@inheritdoc}
   */
  public function block() {
    return $this->user->block();
  }

  /**
   * {@inheritdoc}
   */
  public function getInitialEmail() {
    return $this->user->getInitialEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function setExistingPassword($password) {
    return $this->user->setExistingPassword($password);
  }

  /**
   * {@inheritdoc}
   */
  public function checkExistingPassword(UserInterface $account_unchanged) {
    return $this->user->checkExistingPassword($account_unchanged);
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator() {
    throw new \Exception('Invalid use of getIterator in token authentication.');
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    $this->user->toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    return $this->user->toLink($text, $rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function isNewTranslation() {
    return $this->user->isNewTranslation();
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadedRevisionId() {
    return $this->user->getLoadedRevisionId();
  }

  /**
   * {@inheritdoc}
   */
  public function updateLoadedRevisionId() {
    return $this->user->updateLoadedRevisionId();
  }

  /**
   * {@inheritdoc}
   */
  public function wasDefaultRevision() {
    return $this->user->wasDefaultRevision();
  }

  /**
   * {@inheritdoc}
   */
  public function isLatestRevision() {
    return $this->user->isLatestRevision();
  }

  /**
   * {@inheritdoc}
   */
  public function isLatestTranslationAffectedRevision() {
    return $this->user->isLatestTranslationAffectedRevision();
  }

  /**
   * {@inheritdoc}
   */
  public function isRevisionTranslationAffectedEnforced() {
    return $this->user->isRevisionTranslationAffectedEnforced();
  }

  /**
   * {@inheritdoc}
   */
  public function setRevisionTranslationAffectedEnforced($enforced) {
    return $this->user->setRevisionTranslationAffectedEnforced($enforced);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultTranslationAffectedOnly() {
    return $this->user->isDefaultTranslationAffectedOnly();
  }

  /**
   * {@inheritdoc}
   */
  public function setSyncing($status) {
    $this->user->setSyncing($status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isSyncing() {
    return $this->user->isSyncing();
  }

}
