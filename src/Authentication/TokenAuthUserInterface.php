<?php

namespace Drupal\oauth2_rs\Authentication;

use Drupal\user\UserInterface;

/**
 * Token Auth decorated User interface.
 *
 * @internal
 */
interface TokenAuthUserInterface extends \IteratorAggregate, UserInterface {

  /**
   * Get the token.
   *
   * @return \Lcobucci\JWT\Token
   *   The provided OAuth2 token.
   */
  public function getToken();

  /**
   * Get the activated consumer.
   *
   * @return \Drupal\consumers\Entity\Consumer
   *   The activated consumer after authentication.
   */
  public function getConsumer();

}
