<?php

namespace Drupal\oauth2_rs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginManager;
use Drupal\oauth2_rs\Service\FileSystemChecker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The settings form.
 *
 * @internal
 */
class Oauth2TokenSettingsForm extends ConfigFormBase {

  /**
   * The file system checker.
   *
   * @var \Drupal\oauth2_rs\Service\FileSystemChecker
   */
  protected $fileSystemChecker;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Authorization validator plugin manager.
   *
   * @var \Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginManager
   */
  protected $validatorTypeManager;

  /**
   * Oauth2TokenSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\oauth2_rs\Service\FileSystemChecker $file_system_checker
   *   The oauth2_rs.filesystem service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    FileSystemChecker $file_system_checker,
    MessengerInterface $messenger,
    AuthorizationValidatorPluginManager $validatorTypeManager
  ) {
    parent::__construct($configFactory);
    $this->fileSystemChecker = $file_system_checker;
    $this->messenger = $messenger;
    $this->validatorTypeManager = $validatorTypeManager;
  }

  /**
   * Creates the form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\oauth2_rs\Entity\Form\Oauth2TokenSettingsForm
   *   The form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('oauth2_rs.filesystem_checker'),
      $container->get('messenger'),
      $container->get('plugin.manager.authorization_validator_plugin')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'oauth2_token_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $configs = ['oauth2_rs.settings'];
    $plugins = $this->validatorTypeManager->getDefinitions();
    foreach ($plugins as $plugin) {
      array_push($configs, $configs[0].'.'.$plugin['id']);
    }
    return $configs;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('oauth2_rs.settings');
    $validator = $form_state->getValue('validator');
    $settings->set('validator', $validator);
    $settings->save();

    $plugin_settings = $this->config('oauth2_rs.settings.' . $validator);
    $plugin_settings->set('jwk_url', $form_state->getValue('jwk_url'));
    $plugin_settings->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Access Token entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oauth2_rs.settings');

    $type_definitions = $this->validatorTypeManager->getDefinitions();
    $type_options = [];
    foreach ($type_definitions as $type_definition) {
      $type_options[$type_definition['id']] = $type_definition['label'];
    }

    $form['validator'] = [
      '#type' => 'select',
      '#options' => $type_options,
      '#title' => $this->t('Validator'),
      '#default_value' => $config->get('validator'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'pluginSelectedCallback'],
        'event' => 'change',
        'wrapper' => 'plugin-config-wrapper',
      ],
    ];

    $form['configuration_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'plugin-config-wrapper',
      ],
    ];

    $form['actions'] = [
      'actions' => [
        '#cache' => ['max-age' => 0],
        '#weight' => 20,
      ],
    ];

    $selected_plugin_id = $form_state->getValue('validator');
    if (empty($selected_plugin_id)) {
      $selected_plugin_id = $config->get('validator');
    }
    if (!empty($selected_plugin_id)) {
      if($jwk_url = $config->get('jwk_url')) {
        $type_plugin = $this->validatorTypeManager->createInstance($selected_plugin_id, ['jwk_url' => $jwk_url]);
      }
      else {
        $type_plugin = $this->validatorTypeManager->createInstance($selected_plugin_id);
      }
      $form['configuration_wrapper'] += $type_plugin->buildConfigForm($form, $form_state);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Plugin selected ajax callback.
   */
  public function pluginSelectedCallback(array $form, FormStateInterface $form_state) {
    return $form['configuration_wrapper'];
  }

  /**
   * Validates if the file exists.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateExistingFile(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#value'])) {
      $path = $element['#value'];
      // Does the file exist?
      if (!$this->fileSystemChecker->fileExist($path)) {
        $form_state->setError($element, $this->t('The %field file does not exist.', ['%field' => $element['#title']]));
      }
      // Is the file readable?
      if (!$this->fileSystemChecker->isReadable($path)) {
        $form_state->setError($element, $this->t('The %field file at the specified location is not readable.', ['%field' => $element['#title']]));
      }
    }
  }

}
