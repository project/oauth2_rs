<?php

namespace Drupal\oauth2_rs\Plugin;

use BadMethodCallException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\oauth2_rs\Service\UserAuthManagerInterface;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;
use Drupal\Component\Plugin\PluginBase;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token;
use League\OAuth2\Server\CryptTrait;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Authorization validator plugin plugins.
 */
abstract class AuthorizationValidatorPluginBase extends PluginBase implements AuthorizationValidatorPluginInterface, ContainerFactoryPluginInterface {

  use CryptTrait, StringTranslationTrait;

  /**
   * Public key.
   *
   * @var \League\OAuth2\Server\CryptKey
   */
  protected $publicKey;

  protected $userAuthManager;

  protected $cache;

  protected $entityTypeManager;

  protected $configFactory;

  protected $httpClient;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    UserAuthManagerInterface $auth_manager,
    CacheBackendInterface $cache,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userAuthManager = $auth_manager;
    $this->cache = $cache;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('oauth2_rs.user_auth_manager'),
      $container->get('cache.data'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * Get Token.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *
   * @return \Lcobucci\JWT\Token
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  public function getToken(ServerRequestInterface $request): Token {
    if ($request->hasHeader('authorization') === FALSE) {
      throw OAuthServerException::accessDenied('Missing "Authorization" header');
    }

    $header = $request->getHeader('authorization');
    $jwt = trim(preg_replace('/^(?:\s+)?Bearer\s/', '', $header[0]));

    try {
      // Attempt to parse and validate the JWT.
      $token = (new Parser())->parse($jwt);
      return $token;
    } catch (InvalidArgumentException $exception) {
      // JWT couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied($exception->getMessage(), NULL, $exception);
    } catch (RuntimeException $exception) {
      // JWR couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied('Error while decoding to JSON', NULL, $exception);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateAuthorization(ServerRequestInterface $request) {
    $token = $this->getToken($request);

    try {
      if ($token->signature()
          ->verify(new Sha256(), $token->payload(), $this->publicKey) === FALSE) {
        throw OAuthServerException::accessDenied('Access token could not be verified');
      }
    } catch (BadMethodCallException $exception) {
      throw OAuthServerException::accessDenied('Access token is not signed', NULL, $exception);
    }

    // Ensure access token hasn't expired.
    if ($token->isExpired(new \DateTimeImmutable())) {
      throw OAuthServerException::accessDenied('Access token has expired');
    }

    return $token;
  }

}
