<?php

namespace Drupal\oauth2_rs\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Authorization validator plugin plugins.
 */
interface AuthorizationValidatorPluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
