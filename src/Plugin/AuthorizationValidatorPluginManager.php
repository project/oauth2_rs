<?php

namespace Drupal\oauth2_rs\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Authorization validator plugin plugin manager.
 */
class AuthorizationValidatorPluginManager extends DefaultPluginManager {


  /**
   * Constructs a new AuthorizationValidatorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/AuthorizationValidators',
      $namespaces,
      $module_handler,
      'Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginInterface',
      'Drupal\oauth2_rs\Annotation\AuthorizationValidator'
    );

    $this->alterInfo('oauth2_rs_authorization_validator_plugin_info');
    $this->setCacheBackend($cache_backend, 'oauth2_rs_authorization_validator_plugins');
  }

}
