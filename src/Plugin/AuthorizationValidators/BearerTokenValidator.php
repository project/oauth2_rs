<?php
declare(strict_types=1);

namespace Drupal\oauth2_rs\Plugin\AuthorizationValidators;

use BadMethodCallException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth2_rs\Annotation\AuthorizationValidator;
use Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginBase;
use Drupal\oauth2_rs\Service\UserAuthManagerInterface;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key\LocalFileReference;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CognitoBearerValidator.
 *
 * @AuthorizationValidator(
 *   id = "bearer_token_validator",
 *   label = @Translation("Bearer Token validator"),
 * )
 */
class BearerTokenValidator extends AuthorizationValidatorPluginBase {

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    UserAuthManagerInterface $auth_manager,
    CacheBackendInterface $cache,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $auth_manager,
      $cache,
      $entity_type_manager,
      $config_factory,
      $http_client
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('oauth2_rs.user_auth_manager'),
      $container->get('cache.data'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('oauth2_rs.settings.' . $this->pluginId);

    $build['jwk_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL bearer'),
      '#description' => $this->t('Description'),
      '#default_value' => $config->get('jwk_url') ?? '',
      '#required' => TRUE,
    ];

    return $build;
  }

  /**
   * Set the public key.
   *
   * @param \League\OAuth2\Server\CryptKey $key
   *   Public key.
   */
  public function setPublicKey(CryptKey $key) {
    $this->publicKey = $key;
  }

  /**
   * {@inheritdoc}
   */
  public function validateAuthorization(ServerRequestInterface $request) {
    if ($request->hasHeader('authorization') === FALSE) {
      throw OAuthServerException::accessDenied('Missing "Authorization" header');
    }

    $header = $request->getHeader('authorization');
    $jwt = trim(preg_replace('/^(?:\s+)?Bearer\s/', '', $header[0]));

    try {
      // Attempt to parse and validate the JWT.
      $token = (new Parser())->parse($jwt);
      $publicKey = LocalFileReference::file($this->publicKey->getKeyPath());
      try {
        if ($token->signature()
            ->verify(new Sha256(), $token->payload(), $publicKey) === FALSE) {
          throw OAuthServerException::accessDenied('Access token could not be verified');
        }
      } catch (BadMethodCallException $exception) {
        throw OAuthServerException::accessDenied('Access token is not signed', NULL, $exception);
      }

      // Ensure access token hasn't expired.
      if ($token->isExpired(new \DateTimeImmutable())) {
        throw OAuthServerException::accessDenied('Access token has expired');
      }
      $claims = $token->claims();

      // Return the request with additional attributes.
      return $request
        ->withAttribute('token', $token)
        ->withAttribute('oauth_access_token_id', $claims->get('jti'))
        ->withAttribute('oauth_client_id', $claims->get('aud'))
        ->withAttribute('oauth_user_id', $claims->get('sub'))
        // TODO get claim names from config?.
        ->withAttribute('oauth_scope', $claims->get('scope'))
        ->withAttribute('oauth_username', $claims->get('username'))
        ->withAttribute('oauth_user_email', $claims->get('email'));
    } catch (InvalidArgumentException $exception) {
      // JWT couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied($exception->getMessage(), NULL, $exception);
    } catch (RuntimeException $exception) {
      // JWR couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied('Error while decoding to JSON', NULL, $exception);
    }
  }

}
