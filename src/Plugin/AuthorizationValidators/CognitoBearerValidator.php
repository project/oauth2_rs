<?php
declare(strict_types=1);

namespace Drupal\oauth2_rs\Plugin\AuthorizationValidators;

use CoderCat\JWKToPEM\JWKConverter;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth2_rs\Annotation\AuthorizationValidator;
use Drupal\oauth2_rs\Authentication\Provider\JWTAuthenticationProvider;
use Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginBase;
use Drupal\oauth2_rs\Service\UserAuthManagerInterface;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CognitoBearerValidator.
 *
 * @AuthorizationValidator(
 *   id = "cognito_bearer_validator",
 *   label = @Translation("Cognito Bearer validator"),
 * )
 */
class CognitoBearerValidator extends AuthorizationValidatorPluginBase {

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    UserAuthManagerInterface $auth_manager,
    CacheBackendInterface $cache,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $auth_manager,
      $cache,
      $entity_type_manager,
      $config_factory,
      $http_client
    );
    $this->setPublicKeys();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('oauth2_rs.user_auth_manager'),
      $container->get('cache.data'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('oauth2_rs.settings.' . $this->pluginId);

    $build['jwk_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JWK URL'),
      '#description' => $this->t('Provide Cognito JWK (JSON Web Token) URL'),
      '#default_value' => $config->get('jwk_url') ?? '',
      '#required' => TRUE,
    ];

    return $build;
  }

  protected function getPublicKey($tokenKid) {
    foreach ($this->publicKeys as $key => $item) {
      if ($item[1] == $tokenKid) {
        $this->publicKey = $item[0];
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function validateAuthorization(ServerRequestInterface $request): ServerRequestInterface {
    $token = parent::getToken($request);

    $tokenKid = $token->headers()->get('kid');
    $this->getPublicKey($tokenKid);

    $token = parent::validateAuthorization($request);

    $username = $token->claims()
      ->get(self::getMapping()[JWTAuthenticationProvider::OAUTH_USERNAME]);

    // Is user doesn't exist by name with access token. I need both of the Tokens.
    $account = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $username]);
    // No User exist AND IdToken is in request - enrich request by Token's data.
    if (empty($account) && ($idToken = $this->getIdToken($request)) instanceof Token) {
      $request = $this->enrichRequest($request, $idToken);
    }

    return $this->enrichRequest($request, $token);
  }

  /**
   * Get Public keys from cognito jwks and convert to PEMs.
   *
   * @throws \CoderCat\JWKToPEM\Exception\Base64DecodeException
   * @throws \CoderCat\JWKToPEM\Exception\JWKConverterException
   */
  public function setPublicKeys() {
    // Public keys cache id.
    $cid = 'oauth2_rs.cognito_validator.public_keys';
    if (is_object($this->cache->get($cid))) {
      $pemsKids = $this->cache->get($cid)->data;
    }
    if (!isset($pemsKids)) {
      $jwkConverter = new JWKConverter();
      // Whether url is set?
      if (!empty($this->configuration['jwk_url'])) {
        $jwksUrl = $this->configuration['jwk_url'];
      }
      // return if not set.
      if (!isset($jwksUrl)) {
        return FALSE;
      }
      $response = $this->httpClient->get($jwksUrl);
      $jwks = $response->getBody()->getContents();

      $jwks = json_decode($jwks, TRUE);
      // Convert JWKs to PEMs.
      $pems = $jwkConverter->multipleToPEM($jwks['keys']);

      // Filter kids only for further token identification.
      $kids = array_map(function ($item) {
        return $item['kid'];
      }, $jwks['keys']);

      // Collect public keys with their kids.
      $pemsKids = array_map(NULL, $pems, $kids);

      $this->cache->set($cid, $pemsKids);

    }
    $this->publicKeys = $pemsKids;

  }

  /**
   * Mapping between request - token keys.
   *
   */
  public static function getMapping(): array {
    return $mapping = [
      JWTAuthenticationProvider::OAUTH_ACCESS_TOKEN_ID => 'jti',
      JWTAuthenticationProvider::OAUTH_CLIENT_ID => 'aud',
      JWTAuthenticationProvider::OAUTH_USER_ID => 'sub',
      JWTAuthenticationProvider::OAUTH_SCOPE => 'scope',
      JWTAuthenticationProvider::OAUTH_USERNAME => 'username',
      JWTAuthenticationProvider::OAUTH_ID_TOKEN_USERNAME => 'cognito:username',
      JWTAuthenticationProvider::OAUTH_ID_TOKEN_EMAIL => 'email',
    ];

  }

  /**
   * Enrich request by the data from Token using mappings.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   * @param \Lcobucci\JWT\Token $token
   *
   * @return \Psr\Http\Message\ServerRequestInterface
   */
  public function enrichRequest(ServerRequestInterface $request, Token $token): ServerRequestInterface {
    $claims = $token->claims();
    $mapping = $this::getMapping();
    $request = $request->withAttribute('token', $token);
    // Apply mapping to request.
    foreach ($mapping as $attr => $claim) {
      if (!empty($claimVal = $claims->get($claim))) {
        if (is_array($claimVal)) {
          $claimVal = reset($claimVal);
        }
        $request = $request->withAttribute($attr, $claimVal);
      }
    }

    // Transform the scope to array.
    if (!empty($requestScope = $request->getAttribute(JWTAuthenticationProvider::OAUTH_SCOPE))) {
      $scopes = explode(' ', str_replace('.', '_', $requestScope));

      // Set new value.
      $request = $request->withAttribute(JWTAuthenticationProvider::OAUTH_SCOPE, $scopes);
    }

    // Return the request with additional attributes.
    return $request;
  }

  /**
   * Get Id Token from request. This is required for Cognito on the first
   * request.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *
   * @return bool|\Lcobucci\JWT\Token
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  protected function getIdToken(ServerRequestInterface $request) {
    // If there are no IdToken in request - softly proceed with the process.
    if ($request->hasHeader('IdToken') === FALSE) {
      return FALSE;
    }

    $header = $request->getHeader('IdToken');
    $jwt = trim(preg_replace('/^(?:\s+)?Bearer\s/', '', $header[0]));

    try {
      // Attempt to parse and validate the JWT.
      $token = (new Parser())->parse($jwt);
      return $token;
    } catch (InvalidArgumentException $exception) {
      // JWT couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied($exception->getMessage(), NULL, $exception);
    } catch (RuntimeException $exception) {
      // JWR couldn't be parsed so return the request as is.
      throw OAuthServerException::accessDenied('Error while decoding to JSON', NULL, $exception);
    }
  }

}
