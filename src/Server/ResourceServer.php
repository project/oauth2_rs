<?php

namespace Drupal\oauth2_rs\Server;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\oauth2_rs\AuthorizationValidators\BearerTokenValidator;
use Drupal\oauth2_rs\AuthorizationValidators\CognitoBearerValidator;
use League\OAuth2\Server\AuthorizationValidators\AuthorizationValidatorInterface;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * OAuth2 Resource server.
 */
class ResourceServer implements ResourceServerInterface {

  /**
   * Authorization bearer token validator.
   *
   * @var null|AuthorizationValidatorInterface
   */
  public $authorizationValidator;

  /**
   * Http Message factory.
   *
   * @var \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface
   */
  protected $messageFactory;

  /**
   * Authorization Validator manager.
   *
   * @var \Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginManager
   */
  protected $authValidatorManager;

  /**
   * New server instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $message_factory
   *   Message factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $authValidatorManager
   *   Authorization Validator manager.
   * @param \League\OAuth2\Server\AuthorizationValidators\AuthorizationValidatorInterface|null $authorization_validator
   *   Authorization bearer token validator.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    HttpMessageFactoryInterface $message_factory,
    PluginManagerInterface $authValidatorManager,
    AuthorizationValidatorInterface $authorization_validator = NULL
  ) {
    $this->messageFactory = $message_factory;
    $this->authorizationValidator = $authorization_validator;
    $this->config = $config_factory->get('oauth2_rs.settings');
    $this->configFactory = $config_factory;
    $this->authValidatorManager = $authValidatorManager;
  }

  /**
   * Get authorization validator.
   *
   * @return \League\OAuth2\Server\AuthorizationValidators\AuthorizationValidatorInterface
   *   Authorization validator.
   */
  protected function getAuthorizationValidator() {
    if ($this->authorizationValidator instanceof AuthorizationValidatorInterface === FALSE) {
      $validator_id = $this->config->get('validator');
      $config = $this->configFactory->get('oauth2_rs.settings.' . $validator_id);
      /** @var \Drupal\oauth2_rs\Plugin\AuthorizationValidatorPluginManager $plugin_manager */
      $this->authorizationValidator = $this->authValidatorManager->createInstance($validator_id, $config->getRawData());
    }

    return $this->authorizationValidator;
  }

  /**
   * {@inheritDoc}
   */
  public function validateAuthenticatedRequest(Request $request) {
    // Create a PSR-7 message from the request that is compatible with the OAuth
    // library.
    $psr7_request = $this->messageFactory->createRequest($request);
    // Augment the request with the access token's decoded data or throw an
    // exception if authentication is unsuccessful.
    // Convert back to the Drupal/Symfony HttpFoundation objects.
    return $this->getAuthorizationValidator()
      ->validateAuthorization($psr7_request);
  }

}
