<?php

namespace Drupal\oauth2_rs\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;

/**
 * Class UserAuthService.
 */
class UserAuthManager implements UserAuthManagerInterface {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Password Generator Interface.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected $passwordGenerator;

  /**
   * Constructs a new UserAuthService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   Password Generator Interface.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->config = $configFactory->get('oauth2_rs');
  }

  /**
   * {@inheritDoc}
   */
  public function createAccount($email, $username, $role, $language = 'en') {
    $user = $this->entityTypeManager->getStorage('user')->create([]);
    // Using password generator.
    $user->setPassword(user_password(10));
    $user->enforceIsNew();
    $user->setEmail($email);
    $user->setUsername($username);
    $user->set("langcode", $language);
    $user->set("preferred_langcode", $language);

    // Assume that only one target role assigned to Consumer.
    $user->addRole(reset($role));

    $user->activate();
    $user->save();
    return $user;
  }

  /**
   * {@inheritDoc}
   */
  public function getUserConsumerByRole($role) {
    $clientConsumer = $this->entityTypeManager->getStorage('consumer')
      ->loadByProperties(['roles' => $role]);
    $clientConsumer = reset($clientConsumer);
    return $clientConsumer;
  }

}
