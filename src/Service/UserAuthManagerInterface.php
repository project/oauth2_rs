<?php

namespace Drupal\oauth2_rs\Service;

/**
 * Interface UserAuth.
 */
interface UserAuthManagerInterface {

  /**
   * Extract Consumer by Role.
   *
   * @param string $role
   *   User role.
   *
   * @return \Drupal\consumers\Entity\Consumer
   *   Consumer entity related with a given role.
   */
  public function getUserConsumerByRole(string $role);

  /**
   * Create User account.
   *
   * @param string $email
   *   Email.
   * @param string $username
   *   User name.
   * @param string $role
   *   Role.
   * @param string $language
   *   Language.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   *   Account object.
   */
  public function createAccount(string $email, string $username, string $role, string $language);

}
