<?php

namespace Drupal\Tests\oauth2_rs\Functional;

/**
 * Trait with methods needed by tests.
 */
trait OauthRsTestTrait {

  protected $publicKey = '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkI3gZbsRQN7k7FmAfcAaKinm+rqzYmzUxj6cPeRanZ
DRmOf39WkXNdkLmG+z3SthrZUoT9ZOVJgVNWHlVT7L4tC5WMRvCwFHKlhbDx71j1yzLoaUgZ9qQwclRBXzGrHW
3ED9J884l31jsQldajiWy7qrDZNAtcBZfGxh2PXFlE424V4LBAB5r8pzfnLylvFmdpaMQnyKJzYs5Zph48BYmH
lnOUyZupK0AFa4ZkCLsy4G0IbnOj+yU7bGmCv5G+BjSieseIQjkc2TLJREfonAwNfwcgGytFYxHQdeGruBZx8m
KcGbpUAWw/YY2it9G6iUJJ0AmKBC8yfuSBHhPTvrfwIDAQAB
-----END PUBLIC KEY-----';

  /**
   * Set up public and private keys.
   */
  public function setUpKeys() {
    $public_key_path = 'private:///public.key';

    file_put_contents($public_key_path, $this->publicKey);

    chmod($public_key_path, 0660);

    /** @var \Drupal\Core\File\FileSystemInterface $filesystem */
    $filesystem = \Drupal::service('file_system');

    $settings = $this->config('oauth2_rs.settings');
    $settings->set('public_key', $filesystem->realpath($public_key_path));
    $settings->save();
  }

  /**
   * Base64 url encode.
   *
   * @param string $string
   *   The string to encode.
   *
   * @return string
   *   The encoded string.
   */
  public static function base64urlencode($string) {
    $base64 = base64_encode($string);
    $base64 = rtrim($base64, "=");
    return strtr($base64, '+/', '-_');
  }

}
