<?php

namespace Drupal\Tests\oauth2_rs\Unit\Authentication\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\oauth2_rs\Authentication\Provider\JWTAuthenticationProvider;
use Drupal\oauth2_rs\PageCache\DisallowOauthRequests;
use Drupal\oauth2_rs\PageCache\OauthRequestPolicyInterface;
use Drupal\oauth2_rs\Server\ResourceServerInterface;
use Drupal\oauth2_rs\Service\UserAuthManagerInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * JWT Authentication provider test.
 */
class JWTAuthenticationProviderTest extends UnitTestCase {

  /**
   * The authentication provider.
   *
   * @var \Drupal\oauth2_rs\Authentication\Provider\JWTAuthenticationProvider
   */
  protected $provider;

  /**
   * The OAuth page cache request policy.
   *
   * @var \Drupal\oauth2_rs\PageCache\OauthRequestPolicyInterface
   */
  protected $oauthPageCacheRequestPolicy;

  /**
   * Setup tests.
   */
  protected function setUp():void {
    parent::setUp();

    $resource_server = $this->prophesize(ResourceServerInterface::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->oauthPageCacheRequestPolicy = new DisallowOauthRequests();
    $user_auth_manager = $this->prophesize(UserAuthManagerInterface::class);
    $config_factory = $this->prophesize(ConfigFactoryInterface::class);
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $request_stack = $this->prophesize(RequestStack::class);

    $this->provider = new JWTAuthenticationProvider(
      $resource_server->reveal(),
      $entity_type_manager->reveal(),
      $this->oauthPageCacheRequestPolicy,
      $user_auth_manager->reveal(),
      $config_factory->reveal(),
      $language_manager->reveal(),
      $request_stack->reveal()
    );
  }

  /**
   * @covers \Drupal\oauth2_rs\Authentication\Provider\JWTAuthenticationProvider::applies
   *
   * @dataProvider hasTokenValueProvider
   */
  public function testHasTokenValue($authorization, $has_token) {
    $request = new Request();

    if ($authorization !== NULL) {
      $request->headers->set('Authorization', $authorization);
    }

    $this->assertSame($has_token, $this->provider->applies($request));
    $this->assertSame(
      $has_token ? OauthRequestPolicyInterface::DENY : NULL,
      $this->oauthPageCacheRequestPolicy->check($request)
    );
  }

  /**
   * Token value provider for different cases.
   */
  public function hasTokenValueProvider() {
    $token = $this->getRandomGenerator()->name();
    $data = [];

    // 1. Authentication header.
    $data[] = ['Bearer ' . $token, TRUE];
    // 2. Authentication header. Trailing white spaces.
    $data[] = ['  Bearer ' . $token, TRUE];
    // 3. Authentication header. No white spaces.
    $data[] = ['Foo' . $token, FALSE];
    // 4. Authentication header. Empty value.
    $data[] = ['', FALSE];
    // 5. Authentication header. Fail: no token.
    $data[] = [NULL, FALSE];

    return $data;
  }

}
